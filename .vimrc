" UTF 8 encoding
set encoding=utf-8

" prevent annoying vi stuff
set nocompatible
set nocp

" detect file type for file specific indenting, syntax, etc.
filetype plugin indent on

" syntax highlighting
syntax on
colorscheme molokai

" General settings
set autoindent                 " Auto indentation
set number                     " Line numbers
set backspace=indent,eol,start " proper backspace
set incsearch                  " Incremental search
set ruler                      " Location in bottom right
set wildmenu                   " Command completion
set showcmd                    " Show incomplete commands in the bottom right
set scrolloff=5                " At least 5 lines above and below the cursor
set expandtab                  " Use spaces instead of tabs
set shiftwidth=4               " Use 4 spaces for audo-indent
set softtabstop=2              " Use 4 spaces for tabs
set smartindent                " Enable smart indentation
set nowrap                     " long lines are one long horizontal line

" backup and persistance settings
set undodir=~/.vim/tmp/undo/
set backupdir=~/.vim/tmp/backup/
set undofile
set history=100
set undolevels=100

" Netrw
" let g:netrw_liststyle=3
" let g:netrw_banner=0
" let g:netrw_browse_split=4
" let g:netrw_winsize=10

" NERDTree settings
map <C-x> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif " close vim if nerdtree is the last window

" NERDTree file highlighting
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
    exec 'autocmd filetype nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
    exec 'autocmd filetype nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

call NERDTreeHighlightFile('py', 'blue', 'none', 'blue', '#151515')
call NERDTreeHighlightFile('js', 'green', 'none', 'green', '#151515')
call NERDTreeHighlightFile('html', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('css', 'cyan', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('png', 'red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('jpeg', 'red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('jpg', 'red', 'none', 'red', '#151515')

" Split settings
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow " Change Where the split panes open from
set splitright

" Folding
set foldmethod=indent
set foldlevel=99
nnoremap <space> za

" Control P
let g:ctrlp_working_path_mode = 'r'

" Gundo
nnoremap <f5> :GundoToggle<CR>

" Tagbar settings
nmap <f8> :TagbarToggle<CR>

" Status line settings
let g:airline#extensions#tabline#enabled=1
let g:airline_theme='simple'

" fugitive graph
command -nargs=* Glg Git! log --graph --pretty=format:'\%h - (\%ad)\%d \%s <\%an>' --abbrev-commit --date=local <args>

" Plugins
call plug#begin()

Plug 'mhinz/vim-startify' " Nice looking start menu
Plug 'tmhedberg/SimpylFold' " Better folding
Plug 'kien/ctrlp.vim' " Ctrlp, fuzzy search
Plug 'scrooloose/nerdtree'
" Plug 'tpope/vim-vinegar' " Vinegar for netrw
Plug 'majutsushi/tagbar' " a simple tagbar plugin
Plug 'sjl/gundo.vim' " Undoing is hard man
Plug 'airblade/vim-gitgutter' " Shows git changes in the 'gutter'
Plug 'vim-airline/vim-airline' " Beautiful status line
Plug 'vim-airline/vim-airline-themes' " Themes for the status line
Plug 'tpope/vim-fugitive' " git baby

call plug#end()
